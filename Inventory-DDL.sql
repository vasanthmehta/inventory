---DDL statements for the Inventory Management System

CREATE TABLE Categories (
    id serial NOT NULL PRIMARY KEY,
    name varchar(255) NOT NULL,
    description TEXT
);

CREATE TABLE customers(
id VARCHAR (36) NOT NULL PRIMARY KEY,
name VARCHAR(255) NOT NULL,
email VARCHAR(255) NOT NULL UNIQUE,
contact_phone VARCHAR(255) NOT NULL,
address VARCHAR(255) NOT NULL,
status varchar(20) NOT NULL DEFAULT 'ACTIVE'
);

CREATE TABLE suppliers(
id VARCHAR (36) NOT NULL PRIMARY KEY,
name VARCHAR(255) NOT NULL,
email VARCHAR(255) NOT NULL UNIQUE,
contact_phone VARCHAR(255) NOT NULL,
address VARCHAR(255) NOT NULL,
status VARCHAR(20) NOT NULL DEFAULT 'ACTIVE'
);

CREATE TABLE products(
  	id SERIAL NOT NULL PRIMARY KEY,  
  	name VARCHAR(255) NOT NULL, 
  	description TEXT,
    category_id INTEGER NOT NULL,
  	price DECIMAL(10,2) NOT NULL,
    date_of_manufacture  DATE NOT NULL,
    brand VARCHAR(64),
    product_code VARCHAR(100),
     CONSTRAINT fk_Products_Categories FOREIGN KEY category_id REFERENCES categories(id)
);


CREATE TABLE orders(
    id VARCHAR(255)  primary key NOT NULL,
    customer_id VARCHAR(36) NOT NULL,
     item_count INTEGER NOT NULL,
     order_date TIMESTAMP NOT NULL,
     status VARCHAR(20) NOT NULL,
     amount DECIMAL(10,2) NOT NULL,
     created_by VARCHAR(255) NOT NULL,
      CONSTRAINT fk_orders_customers FOREIGN KEY (customer_id) REFERENCES customers(id) 
);

CREATE TABLE purchases (
    id VARCHAR(255)  primary key NOT NULL,
   supplier_id VARCHAR(36) NOT NULL,
     item_count INTEGER NOT NULL,
     purchase_date TIMESTAMP NOT NULL,
     status VARCHAR(20) NOT NULL,
     amount DECIMAL(10,2) NOT NULL,
     created_by VARCHAR(255) NOT NULL,
      CONSTRAINT fk_purchases_suppliers FOREIGN KEY (supplier_id) REFERENCES suppliers(id) 
);

CREATE TABLE stocks(
id SERIAL NOT NULL PRIMARY KEY,
product_id INT NOT NULL,
quantity INT NOT NULL,
CONSTRAINT fk_stocks_products FOREIGN KEY (product_id) REFERENCES products(id)
);

CREATE TABLE order_line_items (
    id SERIAL NOT NULL PRIMARY KEY,
    order_id VARCHAR(255) NOT NULL,
    customer_id VARCHAR(36) NOT NULL,
    product_id INTEGER NOT NULL,
    quantity INTEGER NOT NULL,
    price DECIMAL(10,2),
   CONSTRAINT fk_orderlineitems_orders FOREIGN KEY (order_id) REFERENCES orders(id),
   CONSTRAINT fk_orderlineitems_customers FOREIGN KEY (customer_id) REFERENCES customers(id),
   CONSTRAINT fk_orderlineitems_products FOREIGN KEY (product_id) REFERENCES products(id)
);

CREATE TABLE purchase_line_items (
    id SERIAL NOT NULL PRIMARY KEY,
    purchase_id VARCHAR(255) NOT NULL,
    supplier_id VARCHAR(36) NOT NULL,
    product_id INTEGER NOT NULL,
    quantity INTEGER NOT NULL,
    price DECIMAL(10,2),
   CONSTRAINT fk_purchaselineitems_suppliers FOREIGN KEY (supplier_id) REFERENCES suppliers(id),
   CONSTRAINT fk_purchaselineitems_purchases FOREIGN KEY (purchase_id) REFERENCES purchases(id),
  CONSTRAINT fk_purchaselineitems_products FOREIGN KEY (product_id) REFERENCES products(id)
);

---Updates performed post Creation

ALTER TABLE categories ADD CONSTRAINT unique_name UNIQUE(name);
